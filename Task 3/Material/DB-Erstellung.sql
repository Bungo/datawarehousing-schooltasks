CREATE DATABASE etl_example;

USE etl_example;

CREATE TABLE PLZ(
	PlzID int not null auto_increment,
    PlzNumber int,
    City varchar(45),
    Canton varchar(45),
    primary key(PlzID)
);

CREATE TABLE Address (
    AddressID int not null auto_increment,
    LastName varchar(45),
    FirstName varchar(45),
    Address varchar(45),
    PlzID int not null,
    primary key (AddressID),
    constraint FK_Plz foreign key (PlzID)
    references PLZ(PlzID)
);

CREATE TABLE ErrorData(
	ErrorID int not null auto_increment,
	LastName varchar(45),
    FirstName varchar(45),
    Address varchar(45),
    PlzNumber int,
    City varchar(45),
    errorType boolean,
    primary key(ErrorID)
);